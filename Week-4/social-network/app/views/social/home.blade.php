<?php
  $posts = array(
    array("date"=>"04/02/15", "message" => "Hello", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello1", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello2", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello3", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello4", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello5", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello6", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello7", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello8", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello9", "image"=>"images/frog.jpg")
    )
?>


@extends('layouts.master')

@section('title')
Social Network Posts
@stop

@section('content')

<!-- Posts Page -->
      <div class="row">
          <div class="col-sm-4">
            <form>
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter Name">
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="4" cols="10" name="message">Enter Your Posts</textarea>
              </div>
              <div class="form-group">
                <label for="imageFile">File input</label>
                <input type="file" id="imageFile" name="imageFile">
              </div>
              <button type="submit" class="btn btn-info">Post</button> <br>
            </form>
            </br>
          </div>
         <div class="col-sm-8">
            <?php
            // create a random number
            $randomPost = rand(1,5);
            // make the array = random
            $posts[] = $randomPost;
                for($i=0; $i < $randomPost; $i++){
                  $image = $posts[$i]["image"];
                  $message = $posts[$i]["message"];
                  $date = $posts[$i]["date"];
               // echo each item
                  echo "<div class='post'>";
                  echo "<img class='post-image' src='$image' alt='photo'>";
                  echo $message;
                  echo "<br>$date";
                  echo "</div>";
                } 
                
            ?>
              
          </div>
      </div>
      
@stop