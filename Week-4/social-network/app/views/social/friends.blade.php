<?php
    $friends = array(
      array('index' => '00341', 'name' => 'Gough Whitlam', 'address' => 'Loganlea Road, Logan', 'phone number' => '5678 9999', 'email' => 'gough@gough.net'),
      array('index' => '00232', 'name' => 'Alan Koroma', 'address' => 'Logan Road, Mt Gravatt', 'phone number' => '1222 3344', 'email' => 'alan@alan.com'),
      array('index' => '00563', 'name' => 'Christine Milne', 'address' => 'Kessels Road, Nathan', 'phone number' => '2222 3344', 'email' => 'chris@the.treetop'),
      array('index' => '00564', 'name' => 'Tony Abbott', 'address' => 'Smith Street, Brisbane',  'phone number' => '9876 5432', 'email' => 'tony@the.hermitage'),
      array('index' => '00655', 'name' => 'John Howard', 'address' => 'John Street, Nathan', 'phone number' => '7788 9900', 'email' => 'john@howard.com'),
      array('index' => '00136', 'name' => 'Kevin Rudd', 'address' => 'South Street, Brisbane', 'phone number' => '0733 2244', 'email' => 'krudd@queensland.com'),
      array('index' => '00745', 'name' => 'Julia Gillard', 'address' => 'Jones Street, South Bank', 'phone number' => '1234 4321', 'email' => 'julia@the.lodge')
  );
?>


@extends('layouts.master')

@section('title')
Social Network Friends
@stop

@section('content')

<!-- Posts Page -->
      <div class="row">
          <div class="col-sm-4">
            <form>
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter Name">
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="4" cols="10" name="message">Enter Your Posts</textarea>
              </div>
              <div class="form-group">
                <label for="imageFile">File input</label>
                <input type="file" id="imageFile" name="imageFile">
              </div>
              <button type="submit" class="btn btn-info">Post</button> <br>
            </form>
            </br>
          </div>
          <div class="col-sm-8">
            
            <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th style="text-align:center;">Phone Number</th>
                    <th style="text-align:right;">Email</th>
                  </tr>
                </thead>
                <tbody>
                  
                @foreach($friends as $friend)
                 <tr>
                    <td>{{{ $friend['name'] }}}</td>
                    <td style="text-align:center;">{{{ $friend['phone number'] }}}</td>
                    <td style="text-align: right;">{{{ $friend['email'] }}}</td>
                </tr>
                @endforeach

                </tbody>
                </table>
            
          </div>
      </div>
      
@stop