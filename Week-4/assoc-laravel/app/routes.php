<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $name = Input::get('query');
  $address = Input::get('query');
  $phoneNumber = Input::get('query');
  $email = Input::get('query');
  
  
  $mySearch = $name;
  $results = search($name, $address, $phoneNumber, $email);
// pass multiple vars
	return View::make('pms.results')->withPms($results)->withMysearch($mySearch);
});


/* Functions for PM database example. */

/* Search sample data for $name or $address or $phoneNumber from form. */
function search($name, $address, $phoneNumber, $email) {
  
  $pms = getPms();

  // Filter $pms by $name or $address or $sta
    if (!empty($name) || !empty($address) || !empty($phoneNumber) || !empty($email)) {
	     $results = array();
	     foreach ($pms as $pm) {
	         if (stripos($pm['name'], $name) !== FALSE 
	    		    || stripos($pm['address'], $address) !== FALSE
	    		    || strpos($pm['phone number'], $phoneNumber) !== FALSE
	    		    || stripos($pm['email'], $email) !== FALSE ) {
			        $results[] = $pm;
	         }
	     }
	    $pms = $results;
	    
    }

  return $pms;
}