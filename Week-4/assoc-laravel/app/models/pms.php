<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getPms()
{
  $pms = array(
      array('index' => '00341', 'name' => 'Gough Whitlam', 'address' => 'Loganlea Road, Logan', 'phone number' => '5678 9999', 'email' => 'gough@gough.net'),
      array('index' => '00232', 'name' => 'Alan Koroma', 'address' => 'Logan Road, Mt Gravatt', 'phone number' => '1222 3344', 'email' => 'alan@alan.com'),
      array('index' => '00563', 'name' => 'Christine Milne', 'address' => 'Kessels Road, Nathan', 'phone number' => '2222 3344', 'email' => 'chris@the.treetop'),
      array('index' => '00564', 'name' => 'Tony Abbott', 'address' => 'Smith Street, Brisbane',  'phone number' => '9876 5432', 'email' => 'tony@the.hermitage'),
      array('index' => '00655', 'name' => 'John Howard', 'address' => 'John Street, Nathan', 'phone number' => '7788 9900', 'email' => 'john@howard.com'),
      array('index' => '00136', 'name' => 'Kevin Rudd', 'address' => 'South Street, Brisbane', 'phone number' => '0733 2244', 'email' => 'krudd@queensland.com'),
      array('index' => '00745', 'name' => 'Julia Gillard', 'address' => 'Jones Street, South Bank', 'phone number' => '1234 4321', 'email' => 'julia@the.lodge')
  );
  return $pms;
}
?>
