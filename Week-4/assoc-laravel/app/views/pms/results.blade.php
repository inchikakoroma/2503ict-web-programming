

@extends('layouts.master')

@section('title')
Associative array search results page
@stop

@section('content')
     

      <div class="row">
        <div class="col-sm-3">
          <form method="get" action="search">
            
              <h4>Search for Library Users</h4>
              <br>
              <div class="form-group">
              <label for="Search">Name, Address, Phone, Email</label>
              <input type="text" class="form-control" id="name" value="{{$mysearch}}" name="query">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Search">Search</button>
            </div>
          
          </form>
        </div>
       
        <div class="col-sm-9">
            <h4>Results for {{$mysearch}}</h4>
            <br>
            <br>
             
              @if (count($pms) == 0)
             
              <p>No results found.</p>
              
              @else 
             
              
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Library Number</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  
                @foreach($pms as $pm)
                 <tr>
                    <td>{{{ $pm['index'] }}}</td>
                    <td>{{{ $pm['name'] }}}</td>
                    <td>{{{ $pm['address'] }}}</td>
                    <td>{{{ $pm['phone number'] }}}</td>
                    <td>{{{ $pm['email'] }}}</td>
                </tr>
                @endforeach

                </tbody>
                </table>
@endif
          
        </div>
      </div>
      
@stop