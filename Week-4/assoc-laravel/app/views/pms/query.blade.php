@extends('layouts.master')

@section('title')
Associative array search example
@stop

@section('content')

      <div class="row">
        <div class="col-sm-3">
          
          <form method="get" action="search">
            
              <h4>Search for Library Users</h4>
              <br>
            <div class="form-group">
              <label for="Search">Name, Address, Phone, Email</label>
              <input type="text" class="form-control" id="name" value="" name="query">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Search">Search</button>
            </div>
          
          </form>
        
        </div>
       
        <div class="col-sm-9">
          
        </div>
      </div>
      

@stop




