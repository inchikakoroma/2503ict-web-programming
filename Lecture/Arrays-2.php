<?php

    // Arrays as maps

    // Key = name & value is Tom
    $person['name'] = "Tom";
    $person['age'] = 20;
    echo $person['name'], " is ", $person['age'], " years old";
    // same as above
    echo "<br>";
    echo "${person['name']} is ${person['age']} years old";

?>