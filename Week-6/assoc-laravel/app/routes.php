<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */

// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $name = Input::get('name');
  $state = Input::get('name');
  $start = Input::get('name');
  $finish = Input::get('name');
  
  
  $mySearch = $name;
  $pms = search($name, $state, $start, $finish);
// pass multiple vars
	return View::make('pms.results')->withPms($pms)->withMysearch($mySearch);
});


/* Functions for PM database example. */

/* Search sample data for $name or $state or $start or $finish from database. */
function search($name, $state, $start, $finish) {
  
    if (!empty($name) || !empty($state) || !empty($start) || !empty($finish)) {
      // Multiple select statement
  	 $sql =  "SELECT * FROM pms WHERE name like ? or state like ? or start like ? or finish like ?";
	   $pms = DB::select($sql, array("%$name%", "%$state%","%$start%", "%$finish%"));
	  //print_r($pms);
  	 
    } else{
     
     // ASK ABOUT ERRORS
     die("Error:");
    
    } 
	     
	return $pms;

}