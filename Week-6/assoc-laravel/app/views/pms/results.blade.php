

@extends('layouts.master')

@section('title')
Database Search Results
@stop

@section('content')
     

      <div class="row">
        <div class="col-sm-3">
          <form method="get" action="search">
            
              <h4>Search Prime-Ministers</h4>
              <br>
              <div class="form-group">
              <label for="Search">Name, State, Year</label>
              <input type="text" class="form-control" id="name" value="{{$mysearch}}" name="name">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Search">Search</button>
            </div>
          
          </form>
        </div>
       
        <div class="col-sm-9">
            <h4>Results for {{$mysearch}}</h4>
            <br>
            <br>
        
              @if (count($pms) == 0)
             
              <p>No results found.</p>
              
              
              @else 
             
              
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Party</th>
                    <th>Duration</th>
                    <th>State</th>
                    <th>Start</th>
                    <th>Finish</th>
                  </tr>
                </thead>
                <tbody>
                  
                @foreach($pms as $pm)
                 <tr>
                    <td>{{{ $pm->number }}}</td>
                    <td>{{{ $pm->name }}}</td>
                    <td>{{{ $pm->party }}}</td>
                    <td>{{{ $pm->duration }}}</td>
                    <td>{{{ $pm->state }}}</td>
                    <td>{{{ $pm->start }}}</td>
                    <td>{{{ $pm->finish }}}</td>
                </tr>
                @endforeach

                </tbody>
                </table>
@endif
          
        </div>
      </div>
      
@stop