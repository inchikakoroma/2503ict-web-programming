@extends('layouts.master')

@section('title')
Item Detail
@stop

@section('content')


<div class="row">
        <div class="col-sm-6">
        <h4>{{{ $item->summary }}}</h4>
        <p>
        {{{ $item->details }}}
        <p>
        <a class="btn btn-danger" href="{{{ url("delete_item_action/$item->id") }}}" role="button">Delete Item</a>
        <a class="btn btn-info" href="{{{ url("update_item/$item->id") }}}" role="button">Update Item</a>
        <p>
        
        <p>
         
        
        </div>
       
        <div class="col-sm-6">
          
        </div>
      </div>

@stop