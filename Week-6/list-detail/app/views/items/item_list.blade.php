@extends('layouts.master')

@section('title')
Item List
@stop



<div class="row">
        <div class="col-sm-11">
         @section('content')
            <h4>Items</h4>
                
                @if ($items)
                <ul class="list-group">
                    @foreach ($items as $item)
                    <a href="{{{ url("item_detail/$item->id") }}}" class="list-group-item">{{{ $item->summary }}}</a>
                    @endforeach
                </ul>
                @else
                    <p>No items found.</p>
                @endif
                
                <a class="btn btn-info" href="{{{ url("add_item") }}}" role="button">Add Item</a>
        @stop
          
        
        </div>
        
        <div class="col-sm-1">
            
        </div>
</div>