@extends('layouts.master')

@section('title')
Update Item
@stop

@section('content')

  <div class="row">
        <div class="col-sm-3">
          
          <form method="post" action="{{{ url('update_item_action') }}}">
            
              <h4>Update Item</h4>
              <br>
            
              <input type="hidden" name="id" value="{{{ $item->id }}}">
            <div class="form-group">
              <label for="summary">Name</label>
              <input type="text" class="form-control" id="summary" value="{{{ $item->summary }}}" name="summary">
            </div>
            <div class="form-group">
              <label for="details">Details</label>
              <textarea class="form-control" id="details" name="details" rows="3">{{{ $item->details }}}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Update Item">Update Item</button>
            </div>
          
          </form>
        
        </div>
       
        <div class="col-sm-9">
          
        </div>
      </div>

@stop


