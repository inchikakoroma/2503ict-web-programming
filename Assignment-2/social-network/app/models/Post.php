<?php

class Post extends Eloquent{
    
      
    public static $rules = array(
        'title' => 'required',
        'message' => 'required'
    );
    
    
    function comment(){
        return $this->hasMany('Comment');
    }
    
    function user(){
        return $this->belongsTo('User');
    }
    
}