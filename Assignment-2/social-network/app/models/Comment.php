<?php
class Comment extends Eloquent{
   
    public static $rules = array(
        'comment' => 'required'
    );
    
    function post(){
        return $this->belongsTo('Post');
    }
    
}
