<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;


class User extends Eloquent implements UserInterface, RemindableInterface,  StaplerableInterface{

	use UserTrait, RemindableTrait, EloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	
	public function __construct(array $attributes = array()) {
         
         $this->hasAttachedFile('image', [
             'styles' => [
             'medium' => '300x300',
             'thumb' => '100x100'
            ]
        ]);
        
        parent::__construct($attributes);
 }


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public static $createRules = array(
        'email' => 'required|unique:users|min:5',
        'password' => 'required|min:5'
    );
    
    public static $loginRules = array(
        'email' => 'required|min:5',
        'password' => 'required|min:5'
    );
    
    public static $editRules = array(
        'name' => 'required'
    );
    
    
    function post(){
        return $this->hasMany('Post');
    }
    
    function friend(){
        return $this->hasMany('Friend');
    }
    
    

}
