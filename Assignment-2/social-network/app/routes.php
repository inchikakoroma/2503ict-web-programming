<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.

find a post 
find its comments
|
*/

// login route

//get
Route::get('user/login', function() {
  return View::make('users.login');
});


//post
Route::post('user/loginUser', 'UserController@loginUser');
Route::post('user/addFriend', 'UserController@addFriend');
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));


// search route

Route::post('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));
//Route::get('user/search', function() {return View::make('user.search');});


Route::resource('post', 'PostController');
Route::resource('comments', 'CommentController');
Route::resource('user', 'UserController');

// make the home page
//Route::get('/', 'ProductController@index');
Route::get('/', array('as'=>'home','uses'=>'PostController@index'));
