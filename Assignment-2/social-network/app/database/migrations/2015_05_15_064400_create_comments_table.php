<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create a manufacturers table
		Schema::create('comments',
		function($table){
			$table->increments('id');
			$table->string('commentmsg');
			// relationship with posts 
			$table->integer('post_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// delete the manufacturers table
		Schema::drop('comments');
	}

}
