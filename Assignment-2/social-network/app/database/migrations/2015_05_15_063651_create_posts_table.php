<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create a posts table
		Schema::create('posts',
		function($table){
			$table->increments('id');
			$table->string('title');
			$table->string('message');
			$table->string('privacy');
			$table->integer('user_id');
			
		// uses later	
			//$table->integer('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// delete the products table
		Schema::drop('posts');
	}

}
