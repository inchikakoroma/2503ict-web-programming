<?php

class PostsTableSeeder extends Seeder{
    
    public function run(){
        $post = new Post;
        $post->title = 'Today is monday';
        $post->message = "monday";
        $post->privacy = "private";
        $post->user_id = 1;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is Tuesday';
        $post->message = "tuesday";
        $post->privacy = "public";
        $post->user_id = 3;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is wednesday';
        $post->message = "wednesday";
        $post->privacy = "private";
        $post->user_id = 3;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is Thursday';
        $post->message = "thursday";
        $post->privacy = "friends";
        $post->user_id = 2;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is Friday';
        $post->message = "friday";
        $post->privacy = "private";
        $post->user_id = 4;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is Saturday';
        $post->message = "saturday";
        $post->privacy = "friends";
        $post->user_id = 2;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is Sunday';
        $post->message = "sunday";
        $post->privacy = "friends";
        $post->user_id = 4;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is good';
        $post->message = "good day";
        $post->privacy = "private";
        $post->user_id = 2;
        $post->save();
        
        $post = new Post;
        $post->title = 'Today is very good';
        $post->message = "very good day";
        $post->privacy = "public";
        $post->user_id = 1;
        $post->save();
        
        
    }
}