<?php

class CommentsTableSeeder extends Seeder{
    public function run(){
        
        $comment = new Comment;
        $comment->commentmsg = 'nice public';
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'great';
        $comment->post_id = 2;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'monday is good';
        $comment->post_id = 2;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'monday is great';
        $comment->post_id = 1;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'hmmm';
        $comment->post_id = 3;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'good post';
        $comment->post_id = 4;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'lovely';
        $comment->post_id = 5;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'great work';
        $comment->post_id = 6;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'sad...';
        $comment->post_id = 10;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'Ginger Beer';
        $comment->post_id = 9;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'Pringles';
        $comment->post_id = 7;
        $comment->save();
        
        $comment = new Comment;
        $comment->commentmsg = 'Water';
        $comment->post_id = 6;
        $comment->save();
    }
}