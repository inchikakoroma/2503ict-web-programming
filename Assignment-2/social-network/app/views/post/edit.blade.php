@extends('post.layout')

@section('title')
Edit
@stop

@section('content')

    <div class="col-sm-1">
           
            
          
    </div>

    <div class="col-sm-9">
        
        <br>
        <br>
        
            {{ Form::model($post, array('method' => 'PUT', 
            'route' => array('post.update', $post->id))) }}

              <div class="form-group">
                  {{ Form::label('title', 'Post Title: ') }}
                 {{Form::text('title', $post->title, array('class' => 'form-control')) }}
                 {{$errors->first('title')}}
              </div>
              <div class="form-group">
                {{ Form::label('message', 'Message: ') }}
                {{Form::text('message', $post->message, array('class' => 'form-control')) }}
                {{$errors->first('message')}}
               
              </div>
              {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
              {{ link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-info']) }}
           
            {{ Form::close() }}
              
          
    </div>
    <div class="col-sm-1">
           
            
          
    </div>



@stop