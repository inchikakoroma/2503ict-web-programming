@extends('product.layout')

@section('title')
Do Not need this page
@stop

@section('content')

        <div class="col-sm-1">
           
        </div>


        <div class="col-sm-9">
            
            
             {{ Form::open(array('action' => 'PostController@store')) }}
             <div class="form-group">
                  {{ Form::label('title', 'Post Title: ') }}
                 {{Form::text('title', '', array('class' => 'form-control')) }}
                 {{$errors->first('title')}}
              </div>
              <div class="form-group">
                {{ Form::label('message', 'Message: ') }}
                {{Form::text('message', '', array('class' => 'form-control')) }}
                {{$errors->first('message')}}
               
              </div>
              
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::close() }}
         
        </div>
        
    <div class="col-sm-1">
           
    </div>



@stop
