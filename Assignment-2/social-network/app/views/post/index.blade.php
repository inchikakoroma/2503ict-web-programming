
@extends('post.layout')

@section('title')
Social Network
@stop

 @section('content')

    <div class="row">
    
             @if (Auth::check())
         <div class="col-sm-4">
             
             <div>
        
       
              
                  <p>
                    <strong>Welcome: {{ Auth::user()->name }}</strong>
                    {{ link_to_route('user.edit', 'Edit Profile', array($userId), array('class' => 'btn btn-info')) }}
                    
                  </p> 
                   
               
         
            
            </div>
            
            {{ Form::open(array('action' => 'PostController@store')) }}
            {{ Form::hidden('user_id', $userId) }}
             <div class="form-group">
                  {{ Form::label('title', 'Post Title: ') }}
                 {{Form::text('title', '', array('class' => 'form-control')) }}
                 {{$errors->first('title')}}
              </div>
              <div class="form-group">
                {{ Form::label('message', 'Message: ') }}
                {{Form::text('message', '', array('class' => 'form-control')) }}
                {{$errors->first('message')}}
               
              </div>
              <div class="form-group">
                {{ Form::select('privacy', [
                   'private' => 'Private',
                   'public' => 'Public',
                   'friends' => 'Friends']
                    ) }}
               
              </div>
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::reset('Clear', ['class' => 'btn btn-info']) }}
              {{ Form::close() }}
            
        </div>
        
        @else
            <div class="col-sm-4">
                
            </div>
   
        @endif
    
        <div class="col-sm-8">
        
            <h4>Posts</h4>
            
                 @if (Auth::id())
                 
                          @if ($posts)
                    <ul class="list-group">
                        @foreach ($posts as $post)
                       <li class="list-group-item">
                           @if ($post->commentCount == 1)
                           <span class="badge">{{{ $post->commentCount }}} Comment</span>
                           @else
                            <span class="badge">{{{ $post->commentCount }}} Comments</span>
                            @endif
                           <div class='post'>
                              <img class='post-image' src='images/frog.jpg' alt='photo'>
                              <br>
                              <strong>Title: {{{ $post->title }}} </strong>
                              <br>
                              <strong>
                                 User Name: 
                                </strong> {{{ $username }}}
                             
                              
                              <br>
                              <strong>
                                 Message: 
                                </strong>
                             {{{ $post->message }}}
                              <br>
                              <strong>
                                 Type: 
                                </strong>
                              {{{ $post->privacy }}}
                              <p>
    
                                 
                              </p>
                               
                           </div>
                            <p>
                                {{ Form::open(array('method' => 'DELETE', 'route' => 
                                 array('post.update', $post->id))) }}
                                     {{ link_to_route('post.edit', 'Edit', array($post->id), array('class' => 'btn btn-info')) }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                 {{ link_to_route('comments.show', 'Comments', array($post->id), array('class' => 'btn btn-info')) }}
                                 {{ Form::close() }}
                            <p>
                       </li>
                        @endforeach
                        
                    </ul>
                    @else
                        <p>No items found.</p>
                    @endif
                   
                 @else
                         @foreach ($publicPosts as $publicPost)
                        <ul class="list-group">
                            
                            <li class="list-group-item">
                               @if ($publicPost->commentCount == 0)
                               <span class="badge">{{{ $$publicPost->commentCount }}} Comment</span>
                               @else
                                <span class="badge">{{{ $publicPost->commentCount }}} Comments</span>
                                @endif
                               <div class='post'>
                                  <img class='post-image' src='images/frog.jpg' alt='photo'>
                                  <br>
                                  <strong>Title: {{{ $publicPost->title }}} </strong>
                                  <br>
                                <strong>
                                 User Name: {{{ $publicPost->name }}}
                                </strong> 
                                  <br>
                                  <strong>
                                   Comment: </strong>
                                 {{{ $publicPost->message }}}
                                  <br>
                                  <strong>
                                   Post Type: </strong>
                                  {{{ $publicPost->privacy }}}
                                  <p>
        
                                     
                                  </p>
                                 
                                 
                           </div>
                            <p>
                                {{ Form::open(array('action' => 'PostController@store')) }}
                                    
                                 {{ link_to_route('comments.show', 'Comments', array($publicPost->id), array('class' => 'btn btn-info')) }}
                                 {{ Form::close() }}
                            <p>
                       </li>
                        
                            @endforeach
                            
                        </ul>
                        
   
                @endif
                   
                
        
        </div>
        
    </div>

 
@stop
