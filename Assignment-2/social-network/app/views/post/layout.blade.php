
<!DOCTYPE html>
<!-- List item. -->
<html>

 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>@yield('title')</title>
    
    <!-- Icon -->
    <link rel="icon" href="{{{ asset('favicon.ico') }}}">
    
    <!-- Custom styles for this template -->
    {{ HTML::style('css/styles.css') }}
    <link rel="stylesheet" href="css/styles.css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>

<body>
  
  <div class="container">  
     
          <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              
                <li>
                  <a href="{{ URL::to('./') }}">Home</a>
                </li>
    
                @if (Auth::check())
                <li>
                  <a href="{{{ url("user/logout") }}}" role="button">Logout</a>
                </li>
                <li>
                    
                </li>
                @else
                
                <li>
                  <a href="{{{ url("user/login") }}}">Login</a>
                </li>
                <li>
                  <a href="{{{ url("user/create") }}}">Create User</a>
                  
                </li>
                @endif

            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
            
            <div class="col-lg-4">
              
              </div>
              
           <div class="col-lg-8">
               {{ Form::open(array('route' => array('user.search'))) }}
              <div class="input-group">
                 {{Form::text('query', '', array('placeholder' => 'Search Users...', 'class' => 'form-control')) }}
                  <span class="input-group-btn">
                    {{ Form::submit('Search', array('class' => 'btn btn-default')) }}
                    {{ Form::close() }}
                  </span>
              </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
          <br>
          <br>
          <br>
   
           
          @section('content')
          
          
          
          @show
          
  </div> <!-- /container -->
  
</body>
</html>