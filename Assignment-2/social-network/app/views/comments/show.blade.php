@extends('post.layout')

@section('title')
Comments
@stop

 @section('content')
 

    <div class="row">
        
       
    
        @if (Auth::check())
        
             <p>
            <strong>Welcome:</strong> {{ Auth::user()->name }}
            
                    
            </p> 
            <br>
         <div class="col-sm-4">
            
          {{ Form::open(array('action' => 'CommentController@store')) }}
            {{ Form::hidden('post_id', $comments[0]->id) }}
             <div class="form-group">
           
                 {{Form::textarea('comment', null, array('class' => 'form-control')) }}
                 {{$errors->first('comment')}}
              </div>
        
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::reset('Clear', ['class' => 'btn btn-info']) }}
              {{ Form::close() }}
            
        </div>
    
        @else
        <div class="col-sm-4">
                
            </div>
   
        @endif
         <div class="col-sm-8">
        
            <div class="panel panel-success">
  
              <div class="panel-heading">Comments</div>
                  <div class="panel-body">
                        <div class='post'>
                        <img class='post-image' src='../images/frog.jpg' alt='photo'>
                        <br>
                       {{{ $comments[0]->title }}}
                        <br>
                      {{{ $comments[0]->message }}}
                    </div>
                  </div>
            
                <!-- Comments -->
              <ul class="list-group">
                
                @foreach ($comments as $myComment)
                <li class="list-group-item">
                 
                  <p>
                      <strong>
                    Comment: 
                </strong>
                    {{{ $myComment->commentmsg }}}
                  </p>
                  <strong>
                    User's name: 
                </strong>
                  {{ Auth::user()->name }}
                  <p>
                    
                    @if (Auth::check())
                    {{ Form::open(array('method' => 'DELETE', 'route' => array('comments.destroy', $myComment->comment_id))) }}
                      {{ Form::hidden('id', $myComment->comment_id) }}
                    {{ Form::submit('Delete this Nerd', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                      @endif
                 
                  </p>
                </li>
                @endforeach
               
              </ul>
             
              
              
               
            </div>
                           
        </div>
        
    </div>

 
@stop
