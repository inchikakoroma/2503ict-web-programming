@extends('users.layout')

@section('title')
Create User
@stop

 @section('content')
    
    <div class="col-sm-1">
           
        </div>


        <div class="col-sm-10">
                
                <h3>Create New User</h3>
                <br>
                <br>
            {{ Form::open(array('action' => 'UserController@store', 'files' => true)) }}
             
             <div class="form-group">
                  {{ Form::label('email', 'User Name: ') }}
                 {{ Form::text('email', '', array('class' => 'form-control')) }}
                 {{ $errors->first('email') }}
              </div>
              <div class="form-group">
                {{ Form::label('password', 'Password: ') }}
                {{ Form::password('password', array('class' => 'form-control')) }}
                {{ $errors->first('password')}}
                </div>
                <div class="form-group">
                    {{ Form::label('name', 'Name: ') }}
                 {{ Form::text('name', '', array('class' => 'form-control')) }}
                 {{ $errors->first('email') }}
                 </div>
                 
                 <div class="form-group">
                  {{ Form::label('dateofBirth', 'Date Of Birth: ') }}
                {{ Form::selectRange('dayofBirth', 01, 31, array('class' => 'form-control')) }}
                {{ Form::selectMonth('monthofBirth', array('class' => 'form-control')) }}
                {{ Form::selectYear('yearofBirth', 1900, 2070, array('class' => 'form-control'))}}
                 {{ $errors->first('dateofBirth')}}
                 {{ Form::hidden('age', 1) }}
                 </div>
                 <div class="form-group">
                  {{ Form::label('image', 'Image: ') }}
                {{ Form::file('image') }}
                  </div>   
   
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::close() }}
         
        </div>
        
    <div class="col-sm-1">
           
    </div>
 
 @stop