@extends('users.layout')

@section('title')
Login
@stop

 @section('content')
 
          <div class="container">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Login In</div>
                        
                    </div>
                    
                     @if (!Auth::check())
                     
                      @endif 
                           {{ Session::get('login_error') }}
                           
                           {{ Session::forget('login_error') }}
                      
                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                           
                        
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::open(array('url' => secure_url('user/loginUser'))) }}
                                {{Form::text('email', '', array('class' => 'form-control')) }}
                                {{$errors->first('email')}}                                      
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                  {{ Form::password('password', array('class' => 'form-control')) }} 
                                  {{$errors->first('password')}}
                            </div>
                                  
                             
                            
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      {{ Form::submit('Sign in', array('class' => 'btn btn-success')) }}
                                      {{ Form::close() }}
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <br>
                                            Don't have an account! 
                                        <a href="{{{ url("user/create") }}}" role="button">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>    
                              



                        </div>                     
                    </div>  
    
       
    </div>
 
 @stop
 
