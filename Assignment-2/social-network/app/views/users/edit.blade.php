@extends('users.layout')

@section('title')
Edit User
@stop

@section('content')

    <div class="col-sm-1">
           
            
          
    </div>

    <div class="col-sm-9">
        
        <br>
        <br>
        
            {{ Form::model($user, array('method' => 'PUT', 
            'route' => array('user.update', $user->id))) }}

              <div class="form-group">
                  {{ Form::label('name', 'User Name: ') }}
                  {{ Form::text('name', $user->name, array('class' => 'form-control')) }}
                 {{ $errors->first('name') }}
              </div>
               
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::close() }}
              
          
    </div>
    <div class="col-sm-1">
           
            
          
    </div>



@stop