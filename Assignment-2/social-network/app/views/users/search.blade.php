@extends('users.layout')

@section('title')
Database Search Results
@stop

@section('content')
     

      <div class="row">
        <div class="col-sm-2">
           @if (Auth::check())
           <p>
                <strong>Welcome: {{ Auth::user()->name }}</strong>
                
            </p>
            @endif
        </div>
       
        <div class="col-sm-10">
           
            <br>
            <br>
             
              @if (count($searchuser) == 0)
             
              <p>No USER found.</p>
              
              
              @else 
              
               
                <ul class="list-group">
                  <li class="list-group-item">
                    @foreach($searchuser as $user)
                     <div class='post'>
                           
                          <br>
                      
                 
                            <strong style="text-align:right; margin-left:20px;">Name:</strong> {{{ $user->name }}}
                            <br>
                            
                          <br>
                              <strong style="text-align:right; margin-left:20px;">Email:</strong> {{{ $user->email }}}
                           <br>
                           <br>
                        </div>
                       
                        <p>
                          {{ link_to_route('user.show', 'Profile', array($user->id), array('class' => 'btn btn-info')) }}
                          @if (Auth::check())
                         
                        @endif
                    
                        </p>
                        
                       
                      
                    @endforeach
                  </li>
                    
                    
                </ul>
      
                    
                
              
              @endif
          
        </div>
      </div>
      
@stop

