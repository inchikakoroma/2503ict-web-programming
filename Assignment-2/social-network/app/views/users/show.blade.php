@extends('users.layout')

@section('title')
{{{ $myusers->name }}}
@stop

 @section('content')
  <div class="row">
    <div class="col-sm-2">
         
      
            
          
    </div>
    <div class="col-sm-10">
           <h4>Profile Info</h4>
            <ul class="list-group">
                
                <li class="list-group-item">
                     
                        <div class='post'>
                          <img class='post-image' src="{{ asset($myusers->image->url('thumb')) }}" alt='photo'>
                          <br>
                        
                            <strong style="text-align:right; margin-left:20px;">Name:</strong> {{{ $myusers->name }}}
                            <br>
                            <strong style="text-align:right; margin-left:20px;">Age:</strong> {{{ $makeAge }}}
                          <br>
                              <strong style="text-align:right; margin-left:20px;">Email:</strong> {{{ $myusers->email }}}
                          <br>
                        </div>
                        <p>
                            {{ Form::open(array('route' => 
                                 array('user.update', $myusers->id))) }}
                                 
                                 {{ link_to_route('user.edit', 'Edit', array($myusers->id), array('class' => 'btn btn-info')) }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                {{ Form::close() }}
                        </p>
                     
                 </li>
                
                 
                
            </ul>
          
    </div>
  </div>
 @stop