<?php

class PostController extends \BaseController {

	// show the index page
	public function index()
	{

			
			$userId = Auth::id();
		
		
			
			if (Auth::check()){
    			$posts = Auth::user()->post;
    			$username = Auth::user()->name;
    			 
    			return View::make('post.index', compact('posts', 'userId', 'username'));
			}else{
				
				$sql = "SELECT *, users.id AS users_id, count(comments.post_id) AS commentCount FROM users LEFT JOIN posts ON users.id = posts.id LEFT JOIN comments ON posts.id = comments.post_id WHERE privacy='public' group by posts.id ORDER BY posts.id DESC;";
				$publicPosts = DB::select($sql);
				
			return View::make('post.index', compact('publicPosts'));
	
			}
 //$sql = "SELECT *, users.id AS users_id, count(comments.post_id) AS commentCount FROM users LEFT JOIN posts ON users.id = posts.id LEFT JOIN comments ON posts.id = comments.post_id WHERE privacy='public' group by posts.id ORDER BY posts.id DESC;";
//$posts = DB::select($sql);
	
	
	
	
	
	//SELECT * FROM posts LEFT JOIN comments ON posts.id = comments.post_id WHERE posts.privacy = 'private';
	
////	SELECT *, users.id AS users_id FROM users LEFT JOIN posts ON users.id = posts.id WHERE posts.privacy = 'private'
	
	}


	// save a post
	public function store()
	{
		// return form inputs
		$input = Input::all();
		
		// Validation
		$v = Validator::make($input, Post::$rules);
		
		// error check
		if($v->passes()){
		
			// Create new Product
			$posts = new Post();
			
			// assign values
			$posts->title = $input['title'];
			$posts->message = $input['message'];
			$posts->privacy = $input['privacy'];
			$posts->user_id = $input['user_id'];
			
			// save Object
			$posts->save();
			
			// redirect after save
			return Redirect::route('post.index');
		
		} else{
			
			// Pass Validation Object (error msgs)
			return Redirect::action('PostController@index')->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
			//$userId = Auth::id();
			$myusers = User::find($id);
			return View::make('post.index')->with('userId', $userId)->with('myusers', $myusers);
	}


	// edit a post
	public function edit($id)
	{
		$post = Post::find($id);
		return View::make('post.edit')->with('post', $post);
	}


	// update a post 
	public function update($id)
	{
		
		// find product to update
		$post = Post::find($id);
		
		$userId = Auth::id();
		
		// return form inputs
		$input = Input::all();
		
		$v = Validator::make($input, Post::$rules);
		
		
		
		// error check
		if($v->passes()){
			
			// assign values
			$post->title = $input['title'];
			$post->message = $input['message'];
			
			// save Object
			$post->save();
			
			// redirect after save
			return Redirect::route('post.index');
			
		} else{
			
			return Redirect::action('PostController@edit', $id)->withInput($input)->withErrors($v);
		
		}
	}


	// delete a post 
	public function destroy($id)
	{
		
		// find product
		$posts = Post::find($id);
		$posts->delete();
		
		// get the comment where the post_id is the id
		$comments = Comment::where('post_id', '=', $id)->get();
		
		// loop through each comment and delete
		foreach($comments as $comment){
			$comment->delete();
		}
		
		// redirect to main page
		return Redirect::route('post.index');
		
	
	}


}


