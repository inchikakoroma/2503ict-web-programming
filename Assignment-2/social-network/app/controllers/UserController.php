<?php

class UserController extends \BaseController {

			public function index()
	{
		return View::make('users.test');
		
	}
		
		
		public function login()
	{
		//login form 
		return View::make('users.login');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// create a new user form 
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// return form inputs
		$input = Input::all();
		
		// Validation
		$v = Validator::make($input, User::$createRules);
		
		
		// error check
		if($v->passes()){
		
			$password = $input['password'];
			// has the input
			$encrypted = Hash::make($password);
			// Create new User
			$user = new User;
			// assign values
			$user->email = $input['email'];
			$user->password = $encrypted;
			$user->name = $input['name'];
			$user->dayofBirth = $input['dayofBirth'];
			$user->monthofBirth = $input['monthofBirth'];
			$user->yearofBirth = $input['yearofBirth'];
			$user->age = $input['age'];
			
			
			$user->image = $input['image'];
			
			// save Object
			$user->save();
			
				
			// redirect to product's index page
			return Redirect::route('post.index');
	
		} else{
			
			// Pass Validation Object (error msgs)
			return Redirect::action('UserController@create')->withErrors($v);
		}
		
	
	}


	// show a user with age
	public function show($id)
	{
		
		
		// make users age
		$myusers = User::find($id);
		
		$makeYear = $myusers->yearofBirth;
		$makeCurrentYear = date('Y');
		$makeAge = $makeCurrentYear - $makeYear;
	//	var_dump($makeAge);
	
		
		return View::make('users.show')->with('myusers', $myusers)->with('makeAge', $makeAge);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	
	// call the edit page
	public function edit($id)
	{
		$user = User::find($id);
		return View::make('users.edit')->with('user', $user);
	}


	// edit a user
	public function update($id)
	{
		// find product to update
		$user = User::find($id);
		
		// return form inputs
		$input = Input::all();
		
		$v = Validator::make($input, User::$editRules);
		
		
		
		// error check
		if($v->passes()){
			
			// assign values
			//$user->email = $input['email'];
			$user->name = $input['name'];
			
			// save Object
			$user->save();
			
			// redirect after save
			return Redirect::route('user.show', $user->id);
			
		} else{

			
			return Redirect::back()->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function loginUser(){
		
		
		$input = Input::all();
		
		$v = Validator::make($input, User::$loginRules);
		
		
		
		// error check
		if($v->passes()){
			
			$userdata = array(
			// get the input form the login form
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		
		// authenticate (check user)
		if(Auth::attempt($userdata)){
			// return to previous page
			return Redirect::action('PostController@index');
		}else{
			// error msg
			 Session::put('login_error', 'Login failed');
			
			// return to previous page
			return Redirect::to(URL::previous())->withInput();
		}
		
			// redirect after save
			return Redirect::route('users.login');
			
		} else{
			
			return Redirect::back()->withErrors($v);
		}
		
	}
	
	public function logout(){
		Auth::logout();
		return Redirect::action('PostController@index');
	}
	
	
	// search for users
	public function search(){
	
		$input = Input::all();
    	
    	$i = $input['query'];
    	$mySearch = $i;
    	
    //	$userImage = User::all();
    	
    	//$searchuser = User::where('email','like',$i)->get();
    	
    	 $sql =  "SELECT * FROM users WHERE email like ? ";
	  	$searchuser = DB::select($sql, array("%$i%"));
	 

 		 return View::make('users.search')->with(compact('mySearch', 'searchuser'));
    //	return View::make('users.search')->with('mySearch',$mySearch)->with('searchuser', $searchuser);
	}
	
	public function getIndex()
  {
    $not_friends = User::where('id', '!=', Auth::user()->id);
    if (Auth::user()->friend->count()) {
      $not_friends->whereNotIn('id', Auth::user()->friend->modelKeys());
    }
    $not_friends = $not_friends->get();
 
    return View::make('user.index')->with('not_friends', $not_friends);
  }

	
	
		public function addFriend($id){
			 $user = User::find($id);
			 Auth::user()->addFriend($user);
			 return Redirect::back();
		}
}
