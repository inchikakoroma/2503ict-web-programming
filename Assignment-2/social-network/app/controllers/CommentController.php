<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		echo "test comments";
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		echo "create a new comment";
	}


	// save a comment
	public function store()
	{
		$input = Input::all();
		
		// validattion
		$v = Validator::make($input, Comment::$rules);
		
		
		// error check
		if($v->passes()){
			
			// create new comment
			$comment = new Comment();
			
			// assign values
			$comment->commentmsg = $input['comment'];
			
			$comment->post_id = $input['post_id'];
			
			// save comment object
			$comment->save();
			
			// redirect after save
			return Redirect::back();
		}else{
			
			// pass validation error message
			return Redirect::back()->withErrors($v);
		}
	}


	// show a comment 
	public function show($id)
	{


	$comments = DB::select('select posts.*, comments.post_id, comments.commentmsg, comments.id as comment_id FROM posts left JOIN comments ON posts.Id = comments.post_id where posts.id = ?', array($id));
	
	return View::make('comments.show')->with('comments', $comments);
	
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	// delete a ccomment
	public function destroy($id)
	{
		
		if($id){ 
    			// SQL delete query
    			$sql = "DELETE from comments WHERE id = ?";
    			DB::delete($sql, array($id));
    			// redirect to list items
   				return Redirect::back();
  		}else{
			    // prinnt error message
			    die("Error deleting item");
		 }
    
	}


}
