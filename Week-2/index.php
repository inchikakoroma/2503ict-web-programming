<?php
  $posts = array(
    array("date"=>"04/02/15", "message" => "Hello", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello1", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello2", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello3", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello4", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello5", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello6", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello7", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello8", "image"=>"images/frog.jpg"),
    array("date"=>"04/02/15", "message" => "Hello9", "image"=>"images/frog.jpg")
    )
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">


    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Photos</a></li>
              <li><a href="#">Friends</a></li>
              <li><a href="#">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main Body -->
      <div class="row">
          <div class="col-sm-4">
            <form>
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter Name">
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="4" cols="10" name="message">Enter Your Posts</textarea>
              </div>
              <div class="form-group">
                <label for="imageFile">File input</label>
                <input type="file" id="imageFile" name="imageFile">
              </div>
              <button type="submit" class="btn btn-info">Post</button> <br>
            </form>
            </br>
          </div>
          <div class="col-sm-8">
            <?php
            // create a random number
            $randomPost = rand(1,5);
            // make the array = random
            $posts[] = $randomPost;
                for($i=0; $i < $randomPost; $i++){
                  $image = $posts[$i]["image"];
                  $message = $posts[$i]["message"];
                  $date = $posts[$i]["date"];
               // echo each item
                  echo "<div class='post'>";
                  echo "<img class='post-image' src='$image' alt='photo'>";
                  echo $message;
                  echo "<br>$date";
                  echo "</div>";
                } 
                
            ?>
              
          </div>
      </div>
       <!-- Random Number -->
      <div class="jumbotron">
        <h3>Task 2</h3>
          <p>
            <a class="btn btn-lg btn-primary" href="../" role="button">Home &raquo;</a>
          </p>
          <?php
                  echo $randomPost;
                  echo "<br>";
                  // !!!Get the Count Of the array!!!
                  $myCount = count($posts);
                  echo $myCount;
          ?>
      </div>

    </div> <!-- /container -->


    
  </body>
</html>