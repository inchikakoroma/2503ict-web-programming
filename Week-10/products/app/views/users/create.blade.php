@extends('users.layout')

@section('title')
Create User
@stop

 @section('content')
    
    <div class="col-sm-1">
           
        </div>


        <div class="col-sm-9">
                
                <h3>Create New User</h3>
                <br>
                <br>
            
              {{ Form::open(array('url' => secure_url('user'))) }}
             <div class="form-group">
                  {{ Form::label('username', 'User Name: ') }}
                 {{ Form::text('username', '', array('class' => 'form-control')) }}
                 {{ $errors->first('username') }}
              </div>
              <div class="form-group">
                {{ Form::label('password', 'Password: ') }}
                {{ Form::password('password', array('class' => 'form-control')) }}
                {{ $errors->first('password')}}
               
              </div>
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
              {{ Form::close() }}
         
        </div>
        
    <div class="col-sm-1">
           
    </div>
 
 @stop