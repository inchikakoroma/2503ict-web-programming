
<!DOCTYPE html>
<!-- List item. -->
<html>

 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>@yield('title')</title>
    
    <!-- Icon -->
    <link rel="icon" href="{{{ asset('favicon.ico') }}}">
    
    <!-- Custom styles for this template -->
    {{ HTML::style('css/styles.css') }}
    <link rel="stylesheet" href="css/styles.css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>

<body>
  
  <div class="container">  
     
          <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Products</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              
                <li><a href="{{ URL::to('./') }}">Home</a></li>
   
                @if (Auth::check())
                <li>
                  <a href="{{{ url("user/logout") }}}" role="button">Logout</a>
                </li>
                @endif
        

            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
       <div class="col-sm-3">
        
       
           @if (Auth::check())
              <p>
                <strong>Welcome:</strong> {{ Auth::user()->username }} 
              </p> 
               
           @else
               {{ Session::get('login_error') }}
           
            <div class="form-signin">
            <h2 class="form-signin-heading">Please sign in</h2>
             <div class="form-group">
               {{ Form::open(array('url' => secure_url('user/login'))) }}
                {{ Form::label('username', 'User Name: ') }}
                {{Form::text('username', '', array('class' => 'form-control')) }}
                {{$errors->first('username')}}
              </div>
              <div class="form-group">
                {{ Form::label('password', 'Password: ') }}
                {{ Form::password('password', array('class' => 'form-control')) }}
                {{$errors->first('password')}}
               
              </div>
       
        {{ Form::submit('Sign in', array('class' => 'btn btn-info')) }}
        {{ Form::close() }}
        <a class="btn btn-info" href="{{{ url("user/create") }}}" role="button">Create User</a>
         </div>
         
        @endif
         
            
    </div>
    <div class="col-sm-9">
           
          @section('content')
          @show
          
          
        
    </div>

      
  </div> <!-- /container -->
  
</body>
</html>