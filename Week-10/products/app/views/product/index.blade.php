
@extends('product.layout')

@section('title')
Products
@stop

 @section('content')
    
    <div class="col-sm-12">
           
            <br>
            <br>
             
              @if (count($products) == 0)
             
              <p>No results found.</p>
             
              @else 
              
               
                <ul class="list-group">
                    @foreach ($products as $product)
                    {{ link_to_route('product.show', $product->name, array($product->id), array('class' => 'list-group-item')) }}
                      
                    @endforeach
                    
                </ul>
                <div class="pagination"> {{ $products->links(); }} </div>
              
                <a class="btn btn-info" href="{{{ url("product/create") }}}" role="button">Add Item</a>

                    
                    <!--
                    <a href="" class="list-group-item">
                      {{ link_to_route('product.show', $product->name, array($product->id)) }}
                      link_to('pr', $title, $attributes = array(), $secure = null);
                      url('product.show', $p = array(), $secure = null);
                    </a>
                    -->

              
              @endif
          
        </div>
 
 @stop