@extends('product.layout')

@section('title')
Edit
@stop

@section('content')

    <div class="col-sm-1">
           
            
          
    </div>

    <div class="col-sm-9">
        
        <br>
        <br>
        
            {{ Form::model($product, array('method' => 'PUT', 
            'route' => array('product.update', $product->id))) }}

              <div class="form-group">
                  {{ Form::label('name', 'Product Name: ') }}
                 {{Form::text('name', $product->name, array('class' => 'form-control')) }}
                 {{$errors->first('name')}}
              </div>
              <div class="form-group">
                {{ Form::label('price', 'Price: ') }}
                {{Form::text('price', $product->price, array('class' => 'form-control')) }}
                {{$errors->first('price')}}
               
              </div>
              {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
           
            {{ Form::close() }}
              
          
    </div>
    <div class="col-sm-1">
           
            
          
    </div>



@stop