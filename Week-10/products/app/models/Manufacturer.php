<?php
class Manufacturer extends Eloquent{
    
    function products(){
        return $this->hasMany('Product');
    }
    
}