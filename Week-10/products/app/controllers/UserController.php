<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// create a user login form 
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// return form inputs
		$input = Input::all();
		
		// Validation
		$v = Validator::make($input, User::$createRules);
		
		
		// error check
		if($v->passes()){
		
			$password = $input['password'];
			// has the input
			$encrypted = Hash::make($password);
			// Create new User
			$user = new User;
			// assign values
			$user->username = $input['username'];
			$user->password = $encrypted;
			
			// save Object
			$user->save();
			
				
			// redirect to product's index page
			return Redirect::route('product.index');
	
		} else{
			
			// Pass Validation Object (error msgs)
			return Redirect::action('UserController@create')->withErrors($v);
		}
		
	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function login(){
		
		
		$input = Input::all();
		
		$v = Validator::make($input, User::$loginRules);
		
		
		
		// error check
		if($v->passes()){
			
			$userdata = array(
			// get the input form the login form
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		
		// authenticate (check user)
		if(Auth::attempt($userdata)){
			// return to previous page
			return Redirect::to(URL::previous());
		}else{
			// error msg
			 Session::put('login_error', 'Login failed');
			
			// return to previous page
			return Redirect::to(URL::previous())->withInput();
		}
		
			// redirect after save
			return Redirect::route('product.show', $product->id);
			
		} else{
			
			return Redirect::back()->withErrors($v);
		}
		
	}

	public function logout(){
		Auth::logout();
		return Redirect::action('ProductController@index');
	}
	
}
