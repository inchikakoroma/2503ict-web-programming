@extends('layouts.master')

@section('title')
Social Network
@stop

 @section('content')

    <div class="row">
    
         <div class="col-sm-4">
             <h4>Create Posts</h4>
             <br>
            <form method="post" action="add_post_action">
              <div class="form-group">
                <label for="name">User Name</label>
                <input type="text" class="form-control" id="summary" value="" name="userName" required>
              </div>
              <div class="form-group">
                <label for="name">Post Titile</label>
                <input type="text" class="form-control" id="summary" value="" name="title" required>
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" id="details" value="" name="message" rows="3" required></textarea>
              </div>
              <button type="submit" class="btn btn-info">Post</button> <br>
            </form>
            </br>
        </div>
    
        <div class="col-sm-8">
        
            <h4>Posts</h4>
                
                @if ($items)
                <ul class="list-group">
                    @foreach ($items as $item)
                   <li class="list-group-item">
                       <span class="badge">{{$item->postCount}} Comments</span>
                       <div class='post'>
                          <img class='post-image' src='images/frog.jpg' alt='photo'>
                          <br>
                          {{{ $item->UserName }}}
                          <br>
                          {{{ $item->Title }}} 
                          <br>
                          {{{ $item->Message }}}
                          <br>
                             
                             
                       </div>
                        <p>
                            <a class="btn btn-info" href="{{{ url("update_post/$item->Id") }}}" role="button">Edit</a>
                            <a class="btn btn-danger" href="{{{ url("delete_post_action/$item->Id") }}}" role="button">Delete</a>
                             <a class="btn btn-info" href="{{{ url("comments/$item->Id") }}}" role="button">Comments</a>
                        <p>
                   </li>
                    @endforeach
                </ul>
                @else
                    <p>No items found.</p>
                @endif
        
        </div>
        
    </div>

 
@stop

