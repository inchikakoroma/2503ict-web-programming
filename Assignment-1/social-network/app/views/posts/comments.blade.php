@extends('layouts.master')

@section('title')
Comments
@stop


 @section('content')

    <div class="row">
    
         <div class="col-sm-4">
             <h4>Create Comment</h4>
             
            <form method="post" action="{{{ url('add_comment_action') }}}">
              <div class="form-group">
                <label for="message">User Name</label>
                <input type="hidden" name="id" value="{{{ $comments[0]->Id }}}">
                <input type="text" class="form-control" id="summary" value="" name="user" required>
              </div>

              <div class="form-group">
                <label for="message">Comment</label>
                <textarea class="form-control" id="comment" value="" name="commentMsg" rows="3" required></textarea>
              </div>
              <button type="submit" class="btn btn-info">Submit</button> <br>
            </form>
            </br>
        </div>
    
        <div class="col-sm-8">
        
            <div class="panel panel-success">
  
              <div class="panel-heading">Comments</div>
              <div class="panel-body">
                <div class='post'>
                    <img class='post-image' src='../images/frog.jpg' alt='photo'>
                    <br>
                    {{{ $comments[0]->UserName }}}
                    <br>
                    {{{ $comments[0]->Title }}} 
                    <br>
                    {{{ $comments[0]->Message }}}
                    </div>
              </div>
            @if ($comments[0]->User)
                <!-- Comments -->
              <ul class="list-group">
                
                @foreach ($comments as $myComment)
                <li class="list-group-item">
                  <p>
                     {{{ $myComment->User }}}
                  </p>
                  <p>
                    {{{ $myComment->CommentMsg }}}
                  </p>
                  <p>
                    <a class="btn btn-danger" href="{{{ url("delete_comment_action/$myComment->comment_id") }}}" role="button">Delete</a>
                  </p>
                </li>
                @endforeach
               
              </ul>
              @else
              <p></p>
              @endif
              
               
            </div>
                           
        </div>
        
    </div>

 
@stop