@extends('layouts.master')

@section('title')
Update Post
@stop

@section('content')

  <div class="row">
        <div class="col-sm-3">
          
          <form method="post" action="{{{ url('update_post_action') }}}">
            
              <h4>Update Post</h4>
              <br>
              <input type="hidden" name="id" value="{{{ $item->Id }}}">
              <input type="text" class="form-control" id="summary" value="{{{ $item->UserName }}}" name="userName">
            <div class="form-group">
              <label for="summary">Post Title</label>
              <input type="text" class="form-control" id="summary" value="{{{ $item->Title }}}" name="title">
            </div>
            <div class="form-group">
              <label for="details">Message</label>
              <textarea class="form-control" id="details" name="message" rows="3">{{{ $item->Message }}}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Update Item">Update Post</button>
              <a class="btn btn-info" href="{{ URL::to('post') }}" role="button">Cancel</a>
            </div>
          
          </form>
        
        </div>
       
        <div class="col-sm-9">
          
        </div>
      </div>

@stop
