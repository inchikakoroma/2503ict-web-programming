@extends('layouts.master')

@section('title')
Add List Item
@stop

@section('content')

         <div class="row">
        <div class="col-sm-3">
          
          <form method="post" action="add_post_action">
            
              <h4>Add New Item</h4>
              <br>
            <div class="form-group">
              <label for="summary">Name</label>
              <input type="text" class="form-control" id="summary" value="" name="summary">
            </div>
            <div class="form-group">
              <label for="details">Details</label>
              <textarea class="form-control" id="details" value="" name="details" rows="3"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Search">Add Item</button>
            </div>
          
          </form>
        
        </div>
       
        <div class="col-sm-9">
          
        </div>
      </div>



@stop