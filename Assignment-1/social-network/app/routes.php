<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Posts Page
|--------------------------------------------------------------------------
*/

Route::get('/', function()
{
	$items = get_posts();
  return View::make('posts.post')->withItems($items);
});


Route::get('post', function()
{
  $items = get_posts();
  return View::make('posts.post')->withItems($items);
});

function get_posts()
{
  $sql = "select Post.*, count(Comment.PosId) AS postCount FROM Post left JOIN Comment ON Post.Id = Comment.PosId group by Post.Id ORDER BY Post.Id DESC;";
  $items = DB::select($sql);
  return $items;
}

/* Add New Post */

Route::post('add_post_action', function()
{
  $userName = Input::get('userName');
  $title = Input::get('title');
  $message = Input::get('message');
  
  $id = add_post($userName, $title, $message);

  // If successfully created then display newly created item
  if ($id) 
  {
    return Redirect::to(url("post/"));
  } 
  else
  {
    die("Error adding item");
  }
});

function add_post($userName, $title, $message) 
{
  $sql = "insert into post (userName, title, message) values (?, ?, ?)";
  DB::insert($sql, array($userName, $title, $message));
  $id = DB::getPdo()->lastInsertId();

  return $id;
}


/* Update Post */

Route::get('update_post/{id}', function($id)
{
  $item = get_post($id);
  return View::make('posts.update_post')->withItem($item);
});

/* Gets  an item with the given id */
function get_post($id)
{
	$sql = "select id, userName, title, message from post where id = ?";
	$items = DB::select($sql, array($id));

	// If we get more than one item or no items display an error
	if (count($items) != 1) 
	{
    die("Invalid query or result");
  }

	// Extract the first item (which should be the only item)
  $item = $items[0];
	return $item;
}

 
Route::post('update_post_action', function()
{
  
  $userName = Input::get('userName');
  $titile = Input::get('title');
  $message = Input::get('message');
  $id = Input::get('id');
  
  
  $pms = updatePost($userName, $titile, $message, $id);
 
   // redirect to item list
  return Redirect::to(url('/post'));
 
});

function updatePost($userName, $title, $message, $id){
  
  // Update post function
  
    $updateItem = "UPDATE post SET userName = ?, title = ?, message = ? WHERE id = ?";
    DB::update($updateItem, array($userName, $title, $message, $id));
   
    return $updateItem;
   
}


/* Delete Post */

Route::get('delete_post_action/{id}', function($id)
{
    if ($id) 
  {
    // SQL delete query
    $sql = "DELETE from post WHERE id = ?";
    DB::delete($sql, array($id));
    // redirect to list items
    return Redirect::to(url('post'));
  } 
  else
  {
    // prinnt error message
    die("Error deleting item");
  }
    
});



/*
|--------------------------------------------------------------------------
| Comments Page
|--------------------------------------------------------------------------
*/

/* Displays post and comments. */
Route::get('comments/{id}', function ($id)
{
  
  $comments = getComment($id);
	return View::make('posts.comments')->withcomments($comments);
});


function getComment($id)
{
	$sql = "select post.*, comment.posId, comment.commentMsg, comment.user, comment.id as comment_id FROM Post left JOIN Comment ON post.Id = Comment.posId where post.id = ?;";
	$comments = DB::select($sql, array($id));
	return $comments;
}

/* Delete Comment */

Route::get('delete_comment_action/{id}', function($id)
{
    if ($id) 
  {
    // SQL delete query
    $sql = "DELETE from comment WHERE id = ?";
    DB::delete($sql, array($id));
    // redirect to list items
   return Redirect::back();
  } 
  else
  {
    // prinnt error message
    die("Error deleting item");
  }
    
});


/* Add Comment */
function addComment($id, $message, $username){
  
    $addComment2 = "insert into Comment (PosId, commentMsg, User) values (?, ?, ?)";
    DB::insert($addComment2, array($id, $message, $username));
   // $id = DB::getPdo()->lastInsertId();
   
}

Route::post('add_comment_action', function()
{
  
  $id = Input::get('id');
  $userName = Input::get('user');
  $message = Input::get('commentMsg');
  
  addComment($id, $userName, $message);
  
  return Redirect::back();
    
});


