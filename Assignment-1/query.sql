/* Social Network database in SQLite. drop table if exists Post; */


CREATE TABLE IF NOT EXISTS Post (
	Id INTEGER PRIMARY KEY autoincrement,
	Title VARCHAR(20) default '' NOT NULL,
	PostDate TIMESTAMP(8),
	Message VARCHAR(255),
	UserName VARCHAR(60));
	
CREATE TABLE IF NOT EXISTS Comment (
	Id INTEGER PRIMARY KEY autoincrement,
	PosId INTEGER NOT NULL REFERENCES Post(Id),
	CommentMsg VARCHAR(255),
	CommentDate TIMESTAMP(8),
	User VARCHAR(60));
	
INSERT INTO Post(Title, Message, UserName)
	VALUES 
	("Post 1", "123 Fake St, Logan", "bob");
INSERT INTO Post(Title, Message, UserName)
	VALUES 
	("Post 2", "1000 Fun St, Nathan", "sally");
INSERT INTO Post(Title, Message, UserName)
	VALUES 
	("Post 3", "700 Friendly St, Woodridge", "john");

INSERT INTO Comment(PosId, CommentMsg, User)
	VALUES 
	(1, "message1", "user1");
INSERT INTO Comment(PosId, CommentMsg, User)
	VALUES 
	(2, "message2", "user2");
INSERT INTO Comment(PosId, CommentMsg, User)
	VALUES 
	(3, "message3", "user3");

select * from Post
order by id;

select * from Comment
order by id;