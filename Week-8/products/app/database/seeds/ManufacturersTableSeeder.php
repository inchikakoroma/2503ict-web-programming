<?php

class ManufacturersTableSeeder extends Seeder{
    
    public function run(){
        $product = new Manufacturer;
        $product->name = 'Bundaberg';
        $product->save();
        
        $product = new Manufacturer;
        $product->name = 'Hans';
        $product->save();
    }
}