
@extends('product.layout')

@section('title')
Products
@stop

 @section('content')
    
    <div class="col-sm-9">
           
            <br>
            <br>
             
              @if (count($products) == 0)
             
              <p>No results found.</p>
             
              @else 
             
              
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  
                @foreach($products as $product)
                 <tr>
                    <td>{{{ $product['name'] }}}</td>
                    <td>{{{ $product['price'] }}}</td>
                   
                </tr>
                @endforeach

                </tbody>
                </table>
@endif
          
        </div>
 
 @stop