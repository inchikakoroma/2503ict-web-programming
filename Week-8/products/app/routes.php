<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::resource('product', 'ProductController');


Route::get('/', function()
{
	
/* one to one relationship */
	//$products = Product::all();
	//$products = Product::find(1);
	//$products = Product::where('price', '>', 2)->get();
	

/* one to many relationship */
	// products that are manufactured by ID (manufacturer_id) 1
	$products = Manufacturer::find(1)->products;
	
	foreach($products as $product)
	    var_dump($product->name);
	    echo"<br><br>";
	   // print_r($products);
	  
	  // find manufacturer of product with ID 1 
	$manufacturer = Product::find(1)->manufacturer;
	var_dump($manufacturer->name);

/* many to many relationship */	   
	    
	  // return View::make('hello');
	   exit();
});
