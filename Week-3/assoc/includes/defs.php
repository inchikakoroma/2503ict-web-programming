<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($name, $year, $state) {
    global $pms; 

    // Filter $pms by $name or $year or $sta
    if (!empty($name) || !empty($year) || !empty($state)) {
	$results = array();
	foreach ($pms as $pm) {
	    if (stripos($pm['name'], $name) !== FALSE 
	    		|| stripos($pm['from'], $year) !== FALSE
	    		|| strpos($pm['to'], $year) !== FALSE
	    		|| stripos($pm['state'], $state) !== FALSE ) {
			$results[] = $pm;
	    }
	}
	$pms = $results;
    }

    return $pms;
}
?>
