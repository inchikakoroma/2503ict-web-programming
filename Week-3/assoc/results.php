<?php
/*
 * Simple example to illustrate associative arrays and queries.
 * DANGEROUS: Does not sanitise user input.
 * BAD STYLE: Does not use templates.  Interleaves PHP and HTML.
 */
include "includes/defs.php";

/* Get form data. */
$name = $_GET['query'];
$year = $_GET['query'];
$state = $_GET['query'];


/* Get array of pms that match query in form data. */
$pms = search($name, $year, $state);
?>

<!-- display results -->
<!DOCTYPE html>
<!-- Home page of PM database search example. -->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Associative Array Search Results</title>

    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
   <link rel="stylesheet" type="text/css" href="styles/wp.css">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <div class="container">  
     
       <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Australian Prime Ministers</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="../">Home</a></li>
              <li><a href="../../Week-2/index.php">Pervious Lab</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <div class="row">
        <div class="col-sm-3">
          <form method="get" action="results.php">
            
              <h4>Search for Prime Ministers</h4>
              <br>
              <div class="form-group">
              <label for="number">Search: (Name, Party, Year)</label>
              <input type="text" class="form-control" id="name" value="<?= $name ?>" name="query">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Search">Search</button>
            </div>
          
          </form>
        </div>
       
        <div class="col-sm-9">
            <h4>Results</h4>
            <br>
            <br>
              <?php 
              if (count($pms) == 0) {
              ?>
              <p>No results found.</p>
              <?php
              } else {
              ?>
              
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Duration</th>
                    <th>Party</th>
                    <th>State</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    foreach($pms as $pm) {
                      // print out the results
                      print 
                      "<tr><td>{$pm['index']}</td><td>{$pm['name']}</td><td>{$pm['from']}</td><td>{$pm['to']}</td><td>{$pm['duration']}</td><td>{$pm['party']}</td><td>{$pm['state']}</td></tr>\n";
                    }
                  ?>
              </tbody>
              </table>
              <?php
              }
              ?>
          
        </div>
      </div>
      
          <hr>
          <p>
          Source:
          <a href="show.php?file=index.html" class="btn btn-info btn-xs" role="button">index.html</a>
          <a href="show.php?file=results.php" class="btn btn-info btn-xs" role="button">results.php</a>
          <a href="show.php?file=includes/defs.php" class="btn btn-info btn-xs" role="button">includes/defs.php</a>
          <a href="show.php?file=includes/pms.php" class="btn btn-info btn-xs" role="button">includes/pms.php</a>
          </p>
     
    </div> <!-- /container -->
  </body>
</html>

