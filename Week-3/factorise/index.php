<!DOCTYPE html>
<!-- Home page for basic factorisation example. -->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Factorisation form</title>

    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
   <link rel="stylesheet" type="text/css" href="styles/wp.css">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <div class="container">  
     
       <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Factorisation</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="../">Home</a></li>
              <li><a href="../../Week-2/index.php">Pervious Lab</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <div class="row">
        <div class="col-sm-6">
          
        </div>
        <div class="col-sm-6">
          <form role="form" method="get" action="factorise.php">
            <div class="form-group">
              <label for="number">Number to Factorise:</label>
              <input type="text" class="form-control" id="number" name="number">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Factorise it">Factorise It</button>
            </div>
          </form>
        </div>
      </div>
  
    </div> <!-- /container -->
  </body>
</html>

