<?php
/*
 * Script to print the prime factors of a single positive integer
 * sent from a form.
 * BAD STYLE: Does not use templates.
 */
include "includes/defs.php";

# Set $number to the value entered in the form.
$number = $_GET['number'];
$myError = '';

# Check $number is nonempty, numeric and between 2 and PHP_MAX_INT = 2^31-1.
# (PHP makes it difficult to do this naturally; see the manual.)
if (empty($number)) {
    // assign the emptyNum to error message
    $myError = "Error: Missing value\n";
} else if (!is_numeric($number)) {
    // assign myError to display error message
    $myError =  "Error: Non-Numeric number: $number\n";
} else if ($number < 2 || $number != strval(intval($number))) {
    // assign inValNuber to display error message
    $myError = "Error: Invalid number: $number\n";
} else {
    # Set $factors to the array of factors of $number.
    $factors = factors($number);
    # Set $factors to a single dot-separated string of numbers in the array.
    $factors = join(" . ", $factors);
}

  // extended::: write to file
  // create a random file
  //$myFile="factors ".date("Y-m-d His").".txt";
  $myFile = "factors.txt";
  $factorFile = fopen($myFile, "a") or die("Unable to write to file");
  $writter = "Factors of $number: $factors\n";
  fwrite($factorFile, $writter);
  fclose($factorFile);
  
  // read from a file
  $readFile = fopen($myFile, "r") or die("uUnable to read from file");
  $fileSize = filesize("factors.txt");
  $fileData = fread($readFile, $fileSize);
  fclose($readFile);
  $reader = "$fileData \n";


?>

<!DOCTYPE html>
<!-- Home page for basic factorisation example. -->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Factorisation form</title>

    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
   <link rel="stylesheet" type="text/css" href="styles/wp.css">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <div class="container">  
     
       <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Factorisation</a></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="../">Home</a></li>
              <li><a href="../../Week-2/index.php">Pervious Lab</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
  
        <div class="row">
        <div class="col-sm-6">
          <?php
          if (empty($myError)){
        ?>
        <p>
          <?php 
            echo "<h3>Factors of <mark>$number</mark>: <br></h3> <h5>$factors</h5>";
          ?>
        </p>
        <?php 
          } else{
        ?>
        <p>
          <?php 
            echo $myError;
          ?>
        </p>
        <?php 
          }
        ?>
        
        <h3>
          Previous Factors
        </h3> 
        <p>
          <?php 
          // insert line breaks when there is a new line
            echo nl2br($reader)
          ?>
        </p>
          
        </p>
        </div>
        <div class="col-sm-6">
          <form role="form" method="get" action="factorise.php">
            <div class="form-group">
              <label for="number">Number to Factorise:</label>
              <input type="text" class="form-control" id="number" name="number" value="<?= $number ?>">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" value="Factorise it">Factorise It</button>
            </div>
          </form>
        </div>
      </div>
      
        <hr>
        <p>
        Sources:
        <a href="show.php?file=factorise.php" class="btn btn-info btn-xs" role="button">factorise.php</a>
        <a href="show.php?file=includes/defs.php" class="btn btn-info btn-xs" role="button">includes/defs.php</a>
    </div> <!-- /container -->
  </body>
</html>