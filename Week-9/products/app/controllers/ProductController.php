<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		$manufacturer = Manufacturer::find(1)->products;
		
		return View::make('product.index', compact('products'), compact('manufacturer')); 

	}

	// Add a new Product Form

	public function create()
	{
		// create a form View
		return View::make('product.create');
	}

	// Store a Product
	
	public function store()
	{
		// return form inputs
		$input = Input::all();
		
		// Validation
		$v = Validator::make($input, Product::$rules);
		
		// error check
		if($v->passes()){
		
			// Create new Product
			$product = new Product();
			
			// assign values
			$product->name = $input['name'];
			$product->price = $input['price'];
			$product->manufacturer_id = 1;
			
			// save Object
			$product->save();
			
			// redirect after save
			return Redirect::route('product.show', $product->id);
		
		} else{
			
			// Pass Validation Object (error msgs)
			return Redirect::action('ProductController@create')->withErrors($v);
		}
		
	}

	// show product info

	public function show($id)
	{
		
	    $product = Product::find($id);
	    return View::make('product.show')->with('product', $product);
	}

	// Edit Product (Edit Form)
	
	public function edit($id)
	{
		$product = Product::find($id);
		return View::make('product.edit')->with('product', $product);
	}


	public function update($id)
	{
	
		// find product to update
		$product = Product::find($id);
		
		// return form inputs
		$input = Input::all();
		
		$v = Validator::make($input, Product::$rules);
		
		
		
		// error check
		if($v->passes()){
			
			// assign values
			$product->name = $input['name'];
			$product->price = $input['price'];
			
			// save Object
			$product->save();
			
			// redirect after save
			return Redirect::route('product.show', $product->id);
			
		} else{
			
		
		return Redirect::action('ProductController@edit', $id)->withInput($input)->withErrors($v);
		
		// Same as Above	
		//	return Redirect::back()->withErrors($v); 
		
		
		}
		
	}

	// Delete a product
	
	public function destroy($id)
	{
		// find product
		$product = Product::find($id);
		// delete
		$product->delete();
		
		// redirect to main page
		return Redirect::route('product.index');
	}


}
