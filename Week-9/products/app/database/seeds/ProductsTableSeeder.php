<?php

class ProductsTableSeeder extends Seeder{
    public function run(){
        $product = new Product;
        $product->name = 'Ginger Beer';
        $product->price = 3.00;
        $product->manufacturer_id = 1;
        $product->save();
        
        $product = new Product;
        $product->name = 'Cheerios';
        $product->price = 2.00;
        $product->manufacturer_id = 1;
        $product->save();
    }
}