@extends('product.layout')

@section('title')
Create
@stop

@section('content')

        <div class="col-sm-1">
           
        </div>


        <div class="col-sm-9">
            
            
              {{ Form::open(array('action' => 'ProductController@store')) }}
             <div class="form-group">
                  {{ Form::label('name', 'Product Name: ') }}
                 {{Form::text('name', '', array('class' => 'form-control')) }}
                 {{$errors->first('name')}}
              </div>
              <div class="form-group">
                {{ Form::label('price', 'Price: ') }}
                {{Form::text('price', '', array('class' => 'form-control')) }}
                {{$errors->first('price')}}
               
              </div>
              {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
          
         
        </div>
        
    <div class="col-sm-1">
           
    </div>



@stop
