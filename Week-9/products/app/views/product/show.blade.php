@extends('product.layout')

@section('title')
{{{ $product->name }}}
@stop

 @section('content')
    
    <div class="col-sm-9">
         <h1>Product</h1>
         <p>Name: {{{ $product->name }}}</p>
         <p>Price: ${{{ $product->price }}}</p>
         
         <p>
             
             {{ Form::open(array('method' => 'DELETE', 'route' => 
                 array('product.update', $product->id))) }}
                 {{ link_to_route('product.edit', 'Edit', array($product->id), array('class' => 'btn btn-info')) }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
         </p>
           
            
            
          
    </div>
    <div class="col-sm-3">
           
            
          
    </div>
 
 @stop